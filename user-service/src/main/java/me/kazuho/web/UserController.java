package me.kazuho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/user")
public class UserController {

  @Value("${server.port}")
  private String port;
  private String protocol = "http://";
  //  instanceId大小写无所谓
  private String instanceId = "other-service";
  private String url = "/other";

  @Autowired
  private RestTemplate restTemplate;

  @GetMapping(value = "/hi")
  public String hi(@RequestParam("name") String name) throws InterruptedException {
    Thread.sleep(3000);
    return "hi, " + name + ", I am from port: " + port;
  }

  @GetMapping(value = "/other")
  public String callOther() {
    String target = protocol + instanceId + url;
    return restTemplate.getForObject(target, String.class);
  }
}
