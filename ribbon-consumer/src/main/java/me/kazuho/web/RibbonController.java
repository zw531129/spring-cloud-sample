package me.kazuho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/ribbon")
public class RibbonController {

  private String protocol = "http://";
  //  instanceId大小写无所谓
  private String instanceId = "user-service";
  private String url = "/user/hi?name=";

  @Autowired
  private RestTemplate restTemplate;


  @GetMapping(value = "/hi")
  public String hi(@RequestParam("name") String name) {
    String target = protocol + instanceId + url + name;
    return restTemplate.getForObject(target, String.class);
  }

}
