//package me.kazuho.filter;
//
//import com.netflix.zuul.ZuulFilter;
//import com.netflix.zuul.context.RequestContext;
//
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpServletRequest;
//
//@Component
//public class CustomFilter extends ZuulFilter {
//  /**
//   * 返回过滤器的类型
//   */
//  @Override
//  public String filterType() {
//    /**
//     * Zuul有一下四种过滤器
//     * "pre":是在请求路由到具体的服务之前执行,这种类型的过滤器可以做安全校验,例如身份校验,参数校验等
//     * "routing":它用于将请求路由到具体的微服务实例,在默认情况下,它使用Http Client进行网络请求
//     * "post":它是在请求已被路由到微服务后执行,一般情况下,用作收集统计信息,指标,以及将响应传输到客户端
//     * "error":它是在其他过滤器发生错误时执行
//     */
//    return "pre";
//  }
//
//  /**
//   * 过滤顺序,值越小,越早执行该过滤器
//   */
//  @Override
//  public int filterOrder() {
//    return 0;
//  }
//
//  /**
//   * 表示该过滤器是否过滤逻辑,如果是ture,则执行run()方法;如果是false,则不执行run()方法.
//   */
//  @Override
//  public boolean shouldFilter() {
//    return true;
//  }
//
//  /**
//   * 具体的过滤逻辑 本例中,检查请求的参数中是否传了token这个参数,如果没传,则请求不被路由到具体的服务实例, 直接返回响应,状态码为401
//   */
//  @Override
//  public Object run() {
//    RequestContext ctx = RequestContext.getCurrentContext();
//    HttpServletRequest request = ctx.getRequest();
//    String token = request.getParameter("token");
//    if (token == null) {
//      ctx.setSendZuulResponse(false);
//      ctx.setResponseStatusCode(401);
//      try {
//        ctx.getResponse().getWriter().write("token is empty");
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//    }
//    return null;
//  }
//}
