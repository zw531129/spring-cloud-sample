//package me.kazuho.fallback;
//
//import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.stereotype.Component;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//
//@Component
//public class CustomFallBack implements FallbackProvider {
//  @Override
//  public String getRoute() {
//    //实现对producer-server服务的熔断
//    return "user-service";
//    //return "*";  实现对所有的路由服务的熔断
//  }
//
//  @Override
//  public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
//    return new ClientHttpResponse() {
//      @Override
//      public HttpStatus getStatusCode() throws IOException {
//        return HttpStatus.OK;
//      }
//
//      @Override
//      public int getRawStatusCode() throws IOException {
//        return 200;
//      }
//
//      @Override
//      public String getStatusText() throws IOException {
//        return "OK";
//      }
//
//      @Override
//      public void close() {
//
//      }
//
//      @Override
//      public InputStream getBody() throws IOException {
//        return new ByteArrayInputStream("fallback".getBytes());
//        //  可以自定义错误消息
////        return new ByteArrayInputStream("{\"status\":500,\"message\":\"sorry,the system goes to sleep,please try again later!\"}".getBytes());
//      }
//
//      @Override
//      public HttpHeaders getHeaders() {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        return headers;
//      }
//    };
//  }
//}
