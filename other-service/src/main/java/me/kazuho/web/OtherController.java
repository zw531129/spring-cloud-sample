package me.kazuho.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OtherController {
  @Value("${server.port}")
  private String port;

  @GetMapping("/other")
  public String other() {
    return "this is other-service, called on port: " + port;
  }
}
